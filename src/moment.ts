export function getDateDiff(olderDate: Date, newerData: Date): 
    { 
      value: number, 
      unit: Intl.RelativeTimeFormatUnit 
    }
  {
  let value: number;
  let unit: Intl.RelativeTimeFormatUnit;

  const timeDiff = Math.round((olderDate.getTime() - newerData.getTime()) / 1000); // time diffrence in seconds

  if (timeDiff < 60) unit = 'second';
  else if (timeDiff < 3600) unit = 'minute';
  else if (timeDiff < 86400) unit = 'hour';
  else if (timeDiff < 2592000) unit = 'day';
  else if (timeDiff < 18144000) unit = 'week';
  else if (timeDiff < 31536000) unit = 'month';
  else if (timeDiff < 94608000) unit = 'quarter';
  else unit = 'year';

  return { value: Math.abs(value), unit };
}
